package fmg.azubi.Logic;

import fmg.azubi.Flight;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class BookingManager {

    private static BookingManager instance;

    private static final int maxBookingID = 10000;
    private ArrayList<Integer> bookingIDs = new ArrayList<>();

    private HashMap<Flight, HashMap<Integer, String>> bookings;

    /**
     * In dieser Methode soll eine eindeutige ID zurückgeben oder -1 falls alle IDs vergeben sind
     * Erstelle mithilfe der Klasse random eine zufällige Zahl zwischen 0 und der maxBookingID
     * Diese ID muss einzigartig sein, also muss überprüft werden ob die ID bereits in der Liste bookingIDs existiert
     * Falls die ID bereits existiert soll erneut versucht werden eine zufällige ID zu erstellen
     * Beträgt die Anzahl der Versuche gleich maxBookingID, kann davon ausgegangen werden, dass alle IDs vergeben sind
     */
    public int getUniqueBookingID(){
        Random random = new Random(System.currentTimeMillis());
        int id = random.nextInt(maxBookingID);
        int retryCount = 0;
        while(bookingIDs.contains(id) && retryCount<maxBookingID) {
            id = random.nextInt(maxBookingID);
            retryCount++;
        }
        if(retryCount == maxBookingID){
            return -1;
        }
        return id;
    }

    /**
     * Erstellt aus dem Flug ein FlightIcon. ImagePath ist dabei SizedImages\{airline Name}.jpg
     * Wenn auf das Icon geklickt wird, soll die GUI den Flug der dahinter steckt markieren
     * Nur falls ein Bild existiert, soll das Icon auch zur Liste hinzugefügt werden
     */
    public String bookingExist(Flight flight, int bookingID){
       HashMap<Integer, String> bookingsForFlight = bookings.get(flight);
       if(bookingsForFlight == null){
           return null;
       }
       String passengerName = bookingsForFlight.get(bookingID);
       return passengerName;
    }

    /**
     * Diese Methode soll einen Flug zu einem Passagier zuweisen und die Buchungs ID zurückgeben
     * Es soll mit der global deklarierten HashMap bookings gearbeitet werden
     * Diese enthält zu den verschiedenen Flügen jeweils eine weitere HashMap mit den Buchungen der Passagiere
     * Gibt es zu einem Flug bereits Buchungen, kann dessen HashMap ausgelesen und weitere Einträge hinzugefügt werden
     * Existieren noch keine Buchungen zu einem Flug, muss für diesen eine neue HashMap angelegt werden
     * In jedem Falle muss eine Buchung, aus dem Passagiernamen und einer eindeutigen ID, in bookings angelegt werden
     */
    public int bookFlight(Flight flight, String passengerName){
        HashMap<Integer, String> bookingForFlight;
        if(bookings.containsKey(flight)){
            bookingForFlight = bookings.get(flight);
        }else{
            bookingForFlight = new HashMap<>();
            bookings.put(flight,bookingForFlight);
        }
        int bookingId = getUniqueBookingID();
        bookingForFlight.put(bookingId,passengerName);

        return bookingId;
    }

    /**
     * Überprüfe ob in bookings der Flug existiert auf dem storniert werden soll
     * Falls ja, entferne die zu stornierende Buchung
     */
    public void cancelFlight(Flight flight, int bookingNumber){
        if(bookings.containsKey(flight)){
            HashMap<Integer, String> bookingForFlight = bookings.get(flight);
            bookingForFlight.remove(bookingNumber);
        }
    }

    private BookingManager(){
        bookings = new HashMap<>();
    }

    public static BookingManager getInstance(){
        if(instance == null){
            instance = new BookingManager();
        }
        return instance;
    }
}
