package fmg.azubi.Logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class IATA_Converter {

    private HashMap<String,String> iataMap;

    private static IATA_Converter instance;

    // getInstance() so lassen?
    public static IATA_Converter getInstance(){
        if(instance == null){
            instance = new IATA_Converter();
        }
        return instance;
    }

    // Konstruktor so lassen?
    private IATA_Converter(){
            iataMap = new HashMap<>();
            loadTSV();
    }

    /**
     * Fülle die iataMap mit den IATA-Codes als Keys und den Flughäfen als Values
     * Die Daten sind in iata.tsv gespeichert und mit Tabstopps getrennt
     */
    private void loadTSV(){
        try {
            BufferedReader tsvReader = new BufferedReader(new FileReader("iata.tsv"));
            String line = "";
            while ((line = tsvReader.readLine()) != null) {
                String[] strings = line.split("\\t");// 0 is IATA Code 1 is the name
                if(iataMap.containsKey(strings[0]) == false) {
                    iataMap.put(strings[0], strings[1]);
                }
            }
        }catch(IOException ioe){
            ioe.printStackTrace();
        }
    }

    /**
     * Gebe den zum IATA-Code zugehörigen Flughafen zurück
     */
    public String getIATA_Name(String iataCode){
        return iataMap.get(iataCode);
    }
}
