package fmg.azubi.GUI;

import fmg.azubi.Flight;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Array;
import java.util.ArrayList;

public class FlightIconPanel extends JPanel {

    ArrayList<FlightIcon> iconList;

    public FlightIconPanel(){
        setBounds(0,100,1500,400);
    }

    /**
     * Gibt die iconList zurück - falls diese null ist, soll sie zuerst initialisiert werden
     */
    public ArrayList<FlightIcon> getIconList(){
        if(iconList == null){
            iconList = new ArrayList<>();
        }
        return iconList;
    }

    /**
     * Erstelle den Setter für die iconList
     */
    public void setIconList(ArrayList<FlightIcon> flightIcons){
        this.iconList = flightIcons;
    }

    /**
     * Entferne alle vorhandenen Icons und zeichne diese neu
     * Die zu zeichnenden Icons sind in iconList gespeichert
     * "Neu zeichnen" bedeutet die Position der Icons zu setzen und zum Panel hinzuzufügen
     * Mit updateUI() kann das Panel aktualisert werden
     */
    public void update(){
        removeAll();
        int count = 0;
        for(FlightIcon icon:iconList){
            icon.setPosition(count*icon.getWidth(),0);
            add(icon);
        }
        updateUI();
    }

    /**
     * Setze die IconList zurück und füge anschließend jeden Flug, in der mitgegebenen Liste flights, zum Panel hinzu
     * Aktualisiere das Panel mithilfe einer internen Methode
     */
    public void setFlights(ArrayList<Flight> flights){
        getIconList().clear();
        for (Flight flight : flights) {
            addFlight(flight);
        }
        update();
    }

    /**
     * Erstellt aus dem Flug ein FlightIcon. ImagePath ist dabei SizedImages\{airline Name}.jpg
     * Wenn auf das Icon geklickt wird, soll die GUI den Flug der dahinter steckt markieren
     * Nur falls ein Bild existiert, soll das Icon auch zur Liste hinzugefügt werden
     */
    public void addFlight(Flight flight){
        FlightIcon icon = new FlightIcon("SizedImages\\" + flight.getAirline()+".jpg",flight);
        icon.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                GUI.getInstance().setFlightForBooking(flight);
            }
        });
        if(icon.getImagePath()!=null) {
            getIconList().add(icon);
        }

    }


}
