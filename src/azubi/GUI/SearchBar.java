package fmg.azubi.GUI;

import fmg.azubi.Flight;
import fmg.azubi.Logic.IATA_Converter;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Locale;

public class SearchBar extends JTextField {
    public static final String flightNumber = "Linie";
    public static final String destinationAirport = "Ziel Flughafen";
    public static final String departTime = "Abflugszeit";

    ArrayList<Flight> flights;
    public SearchBar(ArrayList<Flight> flights){
        this.flights = flights;
    }

    /**
     * Die Methode soll alle Ergebnisse zurückgeben, welche mit dem Text in Textfeld übereinstimmen
     * Falls die Suchleiste leer ist, kann direkt eine leere Liste mit Ergebnissen zurückgegeben werden
     * Überprüfe für jeden Flug ob der Suchtext mit dem Flugtext von Flight.toString() übereinstimmt
     * Verwende den IATA-Converter um auch die IATA-Codes mit in die Suche einzubeziehen
     */
    public ArrayList<Flight> getSearchResults(){
        ArrayList<Flight> results = new ArrayList<>();
        String toSearch = getText().toLowerCase();
        if(toSearch == ""){
            return results;
        }
        String iataSearch = IATA_Converter.getInstance().getIATA_Name(toSearch);

        for(Flight flight: flights){
            String flightString = flight.toString().toLowerCase();
            if(iataSearch != null){
                if(flight.getDestination() == iataSearch.toLowerCase()){
                    results.add(flight);
                }
            }else {
                if (flightString.contains(toSearch)) {
                    results.add(flight);
                }
            }
        }
        return results;
    }

    /**
     * Diese Methode soll eine Liste mit Flügen ausgeben, welche nach einem Kriterium sortiert sind
     * Suche mithilfe von getSearchResults() nach übereinstimmenden Flügen
     * Sortiere diese nach dem ausgewählten Kriterium (Tipp: Suche im Internet nach List.sort und Comparator)
     */
    public ArrayList<Flight> getSearchResults(String sortingAttribute){
        ArrayList<Flight> flightsToSort = getSearchResults();
        flightsToSort.sort(new Comparator<Flight>() {
            @Override
            public int compare(Flight o1, Flight o2) {
                if(sortingAttribute == flightNumber){
                    return o1.getFlightNumber().compareTo(o2.getFlightNumber());
                }else if(sortingAttribute == destinationAirport){
                    return o1.getDestination().compareTo(o2.getDestination());
                }else if(sortingAttribute == departTime){
                    return o1.getStartTime().compareTo(o2.getStartTime());
                }
                return o1.toString().compareTo(o2.toString());
            }
        });
        return flightsToSort;
    }

}
