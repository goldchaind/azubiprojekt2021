package fmg.azubi.GUI;

import fmg.azubi.Flight;

import javax.swing.*;
import java.io.File;

public class FlightIcon extends JLabel {

    private String imagePath;
    private String description;
    private int textHeight = 54;
    private int defaultWidth = 300;
    private int defaultHeight = 200;
    private Flight flight;


    /**
     * Wähle die Eigenschaften des JLabels so, dass der Text unter dem Bild angezeigt wird und der Text zentriert erscheint.
     * Die Flugnummer, Ziel und Startzeit, sollen jeweil in einer separaten Zeile stehen
     * @param imagePath
     * @param flight
     */
    public FlightIcon(String imagePath, Flight flight){
        setDescription("<html><p align=\"center\">"+ flight.getFlightNumber()+"<br>"+flight.getDestination()+"<br>"+flight.getStartTime() + "</p></html>");
        setImagePath(imagePath);
        setHorizontalTextPosition(JLabel.CENTER);
        setVerticalTextPosition(JLabel.BOTTOM);
        this.flight = flight;
    }

    //Verändert die Position auf die Parameter, aber nicht die Größe
    public void setPosition(int x,int y){
        setBounds(x,y,getWidth(),getHeight());
    }

    /**
     * Überprüfe zuerst, ob die angegebene Datei existiert
     * Wenn ja, soll diesem JLabel ein Bild hinzugefügt werden, und die Größe auf die Standartwerte gesetzt
     * @param imagePath
     */
    public void setImagePath(String imagePath){
        File imageFile = new File(imagePath);
        if(imageFile.exists()) {
            this.imagePath = imagePath;
            ImageIcon image = new ImageIcon(imagePath);
            setBounds(0, 0, defaultWidth, defaultHeight);
            setIcon(image);
        }
    }

    /**
     * Getter für imagePath
     */
    public String getImagePath(){
        return imagePath;
    }

    /**
     * Setter für description und angezeigten Text ändern
     */
    public void setDescription(String description){
        this.description = description;
        setText(description);
    }

    /**
     * Getter für flight
     */
    public Flight getFlight(){
        return flight;
    }
}
