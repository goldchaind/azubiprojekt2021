package fmg.azubi.GUI;

import fmg.azubi.Logic.BookingManager;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BookingButton extends JButton implements ActionListener {

    private final static String statusCheck = "Check";
    private final static String statusBook = "Buchen";
    private final static String statusCancel = "Stornieren";

    public BookingButton(){
        setText("Check");
        addActionListener(this);
    }

    public void reset(){
        setText(statusCheck);
    }


    /**
     * Diese Methode wird aufgerufen, wenn der Button geklickt wird.
     * Je nach Status, soll eine andere Aktion ausgeführt werden.
     * Check, überprüft ob die Eingabe aus der GUI einer Zahl oder einem Text entspricht.
     * Handelt es sich um eine Zahl, soll überprüft werden, ob es eine Buchung zu dieser Nummer gibt.
     * In diesem Fall soll in den Status Cancel gewechselt werden, ansonsten soll in den Status Book gewechselt werden
     * Im Status Book, soll der Name der in der Eingabe steht als neue Buchung gespeichert werden
     * Im Status Cancel soll die Buchung storniert werden, also aus dem System gelöscht
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String input = GUI.getInstance().getBookingInput();
        if(getText().equals(statusCheck)) {
            try {

                int bookingNumber = Integer.parseInt(input);
                String passengerNameForBooking = BookingManager.getInstance().bookingExist(GUI.getInstance().getBookingFlight(), bookingNumber);

                if(passengerNameForBooking == null){
                    JOptionPane.showMessageDialog(null,"Buchung wurde nicht gefunden");
                }else {
                    setText(statusCancel);
                    JOptionPane.showMessageDialog(null, "Buchung wurde gefunden und kann ggf. storniert werden");
                }
            } catch (Exception ex) {
                //Non Numeric Value -> Book Flight for Name
                setText(statusBook);
                ex.printStackTrace();

            }
        }else if(getText().equals(statusBook)){
            int bookingID =  BookingManager.getInstance().bookFlight(GUI.getInstance().getBookingFlight(),input);
            JOptionPane.showMessageDialog(null,"Flug gebucht für: "+input+" Buchungsnummer: "+String.valueOf(bookingID));
            reset();

        }else if(getText().equals(statusCancel)){
            int bookingNumber = Integer.parseInt(input);
            BookingManager.getInstance().cancelFlight(GUI.getInstance().getBookingFlight(),bookingNumber);
            JOptionPane.showMessageDialog(null,"Buchung wurde storniert");
            reset();
        }
    }
}
