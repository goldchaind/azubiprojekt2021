package fmg.azubi.Database;


import fmg.azubi.Flight;
import fmg.azubi.Logic.IATA_Converter;

import java.sql.*;
import java.util.ArrayList;

public class DatabaseConnection {

    private static DatabaseConnection instance;
    private static Connection connection;

    private String host;
    private int port;
    private String db;
    private String username;
    private String password;


    /**
     *Gibt die gültige Instanz der Datanbankverbindung zurück bzw. erzeugt diese, falls noch nicht geschehen
     *
     */
    public static DatabaseConnection getInstance(){
        if(instance == null) {
            instance = new DatabaseConnection("devnetddb", 5192, "pdevnetd", "aba_rdo", "aba_rdo#");
        }
        return instance;
    }


    /**
     * Der Konstruktor soll die übergebenen Variablen (parameter) in die entsprechenden globalen
     * Variablen umspeichern.
     */
    private DatabaseConnection(String host, int port, String database, String username, String password) {
        this.host = host;
        this.port = port;
        this.db = database;
        this.username = username;
        this.password = password;
    }

    /**
     * Aufbauen der Verbindung zur Datenbank. Es soll eine Connection erstellt werden, die
     * die Url "jdbc:oracle:thin:@localhost:1521/fmg_test" aufrufen und dabei den Benutzernamen
     * und das Passwort mitgeben. Verwendet dafür die globalen Variablen.
     */
    public Connection getConnection() throws Exception {
        if(connection == null || connection.isClosed()) {
            connection = DriverManager.getConnection("jdbc:oracle:thin:@" + host + ":" + port + "/" + db, username, password);
            System.out.println("Connected to database");
        }
        return connection;
    }

    /**
     * Lesen aller Flugdaten aus Datenbank, bei denen der Abflugort München ist
     * Diese Daten sollen dann als ein Array aus Flight zurückgegeben werden
     * Die Flugnummer und die Airline, können nicht direkt ausgelesen werden, sondern müssen aus dem Attribut "flightnumber" erzeugt werden
     */
    public ArrayList<Flight> getAllFlights() throws Exception{
        PreparedStatement preparedStatement =  getConnection().prepareStatement("select starttime, flightnumber, airplanetype, arrival, runway, flightFrom, flightTo, movingdirection from flugdaten where flightFrom like 'MUC'");
        ResultSet result = preparedStatement.executeQuery();

        ArrayList<Flight> flights = new ArrayList<>();
        while (result.next()) {
            Flight currAirplane = new Flight();
            String flightNumber = result.getString("flightnumber");
            String airline = IATA_Converter.getInstance().getIATA_Name(flightNumber.substring(0,3).trim());
            flightNumber = flightNumber.substring(3);
            currAirplane.setAirline(airline);
            currAirplane.setFlightNumber(flightNumber);
            currAirplane.setStartTime(result.getTimestamp("starttime"));
            currAirplane.setStart(IATA_Converter.getInstance().getIATA_Name(result.getString("flightFrom")));
            currAirplane.setDestination(IATA_Converter.getInstance().getIATA_Name(result.getString("flightTo")));

            flights.add(currAirplane);
        }
        return flights;
    }
}
