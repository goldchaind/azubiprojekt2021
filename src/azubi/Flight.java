package fmg.azubi;


import java.util.Date;

public class Flight {
    String airline;
    String start;
    String flightNumber;
    String destination;
    Date startTime;
    boolean narrowBody;

    // leerer Konstruktor
    public Flight(){

    }

    /**
     * Der Konstruktor soll die übergebenen Variablen (parameter) in die entsprechenden globalen
     * Variablen umspeichern.
     */
    public Flight(String airline, String start, String destination, String flightNumber, boolean narrowBody){
        this.airline = airline;
        this.start = start;
        this.destination = destination;
        this.flightNumber = flightNumber;
        this. narrowBody = narrowBody;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String line) {
        this.flightNumber = line;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setNarrowBody(boolean narrowBody) {
        this.narrowBody = narrowBody;
    }

    public boolean isNarrowBody() {
        return narrowBody;
    }

    /**
     * toString() für folgendes Format:
     * "Airline Flugnummer: Start - Ziel Abflugzeit"
     */
    @Override
    public String toString() {
        return getAirline() +" "+getFlightNumber()+": "+getStart()+" - "+getDestination()+ " "+getStartTime().toString();

    }
}
